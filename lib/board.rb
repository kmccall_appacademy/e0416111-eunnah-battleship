class Board

  attr_accessor :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, val)
    x,y = pos
    @grid[x][y] = val
  end

  # prints the board, with marks for fired upon spaces
  def display
    x_label = []

    # create and print x-axis numerical labels
    x_label << ""
    (0...@grid.length).each do |grid_num|
      x_label << grid_num
    end

    puts x_label.join(" ")

    # create y-axis numerical labels
    @grid.each do |row_array|
      row_array.each do |spot|
        print row_array.to_s
        print "s" if @grid[row_arrow][spot] == :s
        print "x" if @grid[row_arrow][spot] == :x
        print " " if @grid[row_arrow][spot] == nil
      end
      print "\n"
    end
  end

  # returns the number of valid targets (ships) remaining
  def count
    ship_count = 0
    @grid.each do |row_array|
      row_array.each { |spot| ship_count += 1 if spot == :s }
    end
    ship_count
  end

  # checks and returns true if the position is empty
  # if no argument is given, checks if the entire board is empty
  def empty?(pos = nil)
    if pos == nil && count > 0
      false
    elsif pos == nil && count == 0
      true
    else
      true if self[pos] == nil
    end
  end

  # check if the board is completely full
  def full?
    @grid.each do |row_array|
      return false if row_array.any? { |spot| spot == nil }
    end
    true
  end

  def won?
    return true if count == 0
    false
  end

  # randomly distributes ships across the board
  def place_random_ship
    raise "Board is full" if full?

    pos = random_pos

    if empty?(pos)
      self[pos] = :s
    end
  end

  def random_pos
    [rand(@grid.length), rand(@grid[0].length)]
  end

  def in_range?(pos)

  end

end
