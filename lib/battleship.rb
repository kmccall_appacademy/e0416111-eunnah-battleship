# enforce the rules and run the game
# keep a reference to the Player, as well as the Board

class BattleshipGame
  attr_accessor :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  # run the game by calling play_turn until the game is over
  def play
    until game_over
      play_turn
    end
  end

  # get a guess from the player and make an attack
  def play_turn
    pos = @player.get_play
    attack(pos)
    display_status
  end

  # mark the board at pos, destroying or replacing any ship that might be there
  def attack(pos)
    if @board[pos] == :s
      puts "Ship hit!"
      @board[pos] = :x
    elsif @board[pos] == nil
      puts "Your attack missed"
      board[pos] = :x
    else
      puts "You've already attacked this spot!"
    end
  end

  # delegate to the board's count method
  def count
    @board.count
  end

  # delegate to the board's won? method
  def game_over?
    @board.won?
  end

  # print info about the current state of the game, including board state
  # and number of ships remaining
  def display_status
    puts "You won!" if game_over?
    puts "Ships remaining: " + count.to_s
  end


end
