# translates user input into positions of the form [x, y]

class HumanPlayer
  def initalize(name)
    @name = name
  end

  def get_play
    puts "Pick a position to attack, e.g. (3, 4)"
    pos = gets.chomp.split(", ")
    pos.map { |el| el.to_i }
  end
  
end
